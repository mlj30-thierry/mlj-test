#!/bin/bash

### Mis a jour systeme Debian 11
apt update && apt full-upgrade -y

### Installation Apache Mariadb PHP
apt install wget apache2 mariadb-server php -y

### Demarrage services Apache MariaDB
systemctl enable apache2 mariadb

### Installation de Perl
apt install perl -y

### Installation des modules PHP
apt install php-ldap php-imap php-apcu php-xmlrpc php-cas php-mysqli php-mbstring php-curl php-gd php-simplexml php-xml php-intl php-zip php-bz2 -y

### Redemarrage du service Apache
systemctl reload apache2

### Telechargement de GSIT 10.0.0
wget -P /tmp/ https://github.com/DCS-Easyware/gsit/releases/download/GSIT-9.5.7/gsit-9.5.7.tar.gz

### Dezip de GSIT
tar xzvf /tmp/gsit-9.5.7.tar.gz -C /var/www/html

### Modification des droits du dossier GSIT
chown -R www-data:www-data /var/www/html/gsit
chmod -R 775 /var/www/html/gsit

### Creation du mot de passe root de MariaDB
mysql -u root << EOF
create database glpi;
create user glpiadmin@localhost identified by 'GLPI1234';
grant all privileges on glpi.* to glpiadmin@localhost;
flush privileges;
exit
EOF

### Pour finir l'installation
# chmod -R 500 /var/www/html/gsit/config
# rm /var/www/html/gsit/install/install.php

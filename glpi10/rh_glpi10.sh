#!/bin/bash

### Mis a jour systeme
dnf update

### Installation Apache Mariadb
dnf install wget httpd mariadb-server  -y

### Demarrage services Apache MariaDB
systemctl enable httpd mariadb
systemctl start httpd mariadb

### Installation du depot REMI pour PHP
dnf install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm

### Activation du module pour installation de PHP8
dnf module enable php:remi-8.0 -y

### Installation de PHP et des modules
dnf install php php-{mbstring,mysqli,xml,cli,ldap,openssl,xmlrpc,pecl-apcu,zip,curl,gd,json,session,imap} -y

### Redemarrage du service Apache
systemctl reload httpd

### Telechargement de GLPI 10.0.0
wget -P /tmp/ https://github.com/glpi-project/glpi/releases/download/10.0.0/glpi-10.0.0.tgz

### Dezip de GLPI
tar xzf /tmp/glpi-10.0.0.tgz -C /var/www/html

### Modification des droits du dossier GLPI
chown -R apache:apache /var/www/html/glpi
chmod -R 775 /var/www/html/glpi

### Creation du mot de passe root de MariaDB
mysql -u root << EOF
create database glpi;
create user glpiadmin@localhost identified by 'GLPI1234';
grant all privileges on glpi.* to glpiadmin@localhost;
flush privileges;
exit
EOF

### Creation du fichier conf GLPI pour Apache2
touch /etc/httpd/conf.d/glpi.conf

### Parametrage du VHOST pou GLPI
cat << EOF > /etc/httpd/conf.d/glpi.conf
<VirtualHost *:80>
   ServerName server-IP or FQDN
   DocumentRoot /var/www/html/glpi

   ErrorLog "/var/log/httpd/glpi_error.log"
   CustomLog "/var/log/httpd/glpi_access.log" combined

   <Directory> /var/www/html/glpi/config>
           AllowOverride None
           Require all denied
   </Directory>

   <Directory> /var/www/html/glpi/files>
           AllowOverride None
           Require all denied
   </Directory>
</VirtualHost>
EOF

### Parametrage de SELINUX
dnf -y install policycoreutils-python-utils
semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/glpi(/.*)?"
restorecon -Rv /var/www/html/glpi

### Redemarrage du service Apache2
systemctl restart httpd


#### commande a faire apres install
# rm -fr /var/www/html/glpi/install

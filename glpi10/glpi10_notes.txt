#### activer la gestion de l'agent dans GLPI10 ####
GLPI > Administration > Inventaire > Activer l'inventaire

#### pour creer une version portable de l agent GLPI10 ####
- telecharger la derniere version portable de l agent: https://github.com/glpi-project/glpi-agent
- dezip de l agent
- creer un dossier inventaire
- modifier le fichier 'glpi-inventory'
- ajouter a la fin de la ligne 9 : --json > inventaire/%COMPUTERNAME%.json

#### autoriser les reseaux dans la conf de l agent GLPI10 pour linux (sans espace) ####
- editer le fichier /etc/glpi-agent/agent.cfg
- ajouter a la ligne httpd-trust : 127.0.0.1,localhost,ip_du_glpi,ip_de_la_machine1,ip_de_la_machine2,CIDR_du_reseau

#### forcer l'inventaire via l inverface web #####
- aller a l url http://ip_de_la_machine:62354/now

#### forcer l'inventaire via la ligne de commande WINDOWS #####
- glpi-inventory --json > %COMPUTERNAME%.json
- glpi-injector --file %COMPUTERNAME%.json --url http://ip_du_glpi/glpi/

#### forcer l'inventaire via la ligne de commande LINUX #####
- installer curl
- glpi-inventory | curl --data @- http://ip_du_glpi/glpi/

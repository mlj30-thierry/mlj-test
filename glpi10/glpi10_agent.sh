#!/bin/bash

### Mis a jour systeme
apt update && apt full-upgrade -y

### Installation de Perl
apt install perl -y

### Telechargement de GLPI-AGENT 1.2
wget -P /tmp/ https://github.com/glpi-project/glpi-agent/releases/download/1.2/glpi-agent-1.2-linux-installer.pl

### Installation de GLPI-AGENT 1.2
perl /tmp/glpi-agent-1.2-linux-installer.pl

### Ajout des reseaux autorisers dans la conf
cp /etc/glpi-agent/agent.cfg /etc/glpi-agent/agent.cfg.bak
sed -i "s/httpd-trust =/httpd-trust = 127.0.0.1,localhost,10.0.0.0\/24/g" /etc/glpi-agent/agent.cfg

### Redemarre le service GLPI-AGENT
systemctl restart glpi-agent.service
